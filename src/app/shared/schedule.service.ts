import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  constructor(private http: HttpClient) {}

  getVisit(): Observable<any> {
    return this.http.get<any>(
      `${environment.baseUrlVisitas}/visita`
    );
  }

  getVisitByRole(role): Observable<any> {
    return this.http.get<any>(
      `${environment.baseUrlVisitas}/visita?role=${role}`
    );
  }

  postVisit(body): Observable<any> {
    return this.http.post<any>(
      `${environment.baseUrlVisitas}/visita`,body
    );
  }

  putVisit(body): Observable<any> {
    console.log(body);
    return this.http.put<any>(
      `${environment.baseUrlVisitas}/visita/${body.id}`,body
    );
  }

  deleteVisit(id): Observable<any> {
    return this.http.delete<any>(
      `${environment.baseUrlVisitas}/visita/${id}`,
    );
  }
}
 