import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root',
})

export class ClientService {

  constructor(private http: HttpClient) {}

  getclients(filter={}): Observable<any> {
    return this.http.get<any>(
      `${environment.baseUrlVisitasClientes}/cliente`
    );
  }

  getclientsByRole(role): Observable<any> {
    return this.http.get<any>(
      `${environment.baseUrlVisitasClientes}/cliente?role=${role}`
    );
  }

  postClients(body): Observable<any> {
    return this.http.post<any>(
      `${environment.baseUrlVisitasClientes}/cliente`,body
    );
  }

  deleteClients(email): Observable<any> {
    return this.http.delete<any>(
      `${environment.baseUrlVisitasClientes}/cliente/${email}`,
    );
  }
}
