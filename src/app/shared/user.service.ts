import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {}

  getUsers(): Observable<any> {
    return this.http.get<any>(
      `${environment.baseUrlVisitasUsuarios}/usuario`
    );
  }

  getUsersByRole(role): Observable<any> {
    return this.http.get<any>(
      `${environment.baseUrlVisitasUsuarios}/usuario?role=${role}`
    );
  }

  postUser(body): Observable<any> {
    return this.http.post<any>(
      `${environment.baseUrlVisitasUsuarios}/usuario`,body
    );
  }

  deleteuser(email): Observable<any> {
    return this.http.delete<any>(
      `${environment.baseUrlVisitasUsuarios}/usuario/${email}`,
    );
  }
}
 