import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ScheduleAddComponent } from './schedule-add/schedule-add.component';
import { ScheduleService } from '../shared/schedule.service';
import { Router } from '@angular/router';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.page.html',
  styleUrls: ['./schedule.page.scss'],
})
export class SchedulePage implements OnInit {

  public visitas: any = [];
  constructor(
    public scheduleService: ScheduleService,
    private modalController: ModalController,
    public router: Router) { 
      pdfMake.vfs = pdfFonts.pdfMake.vfs;
    }

  ngOnInit() {
    this.getVisit();
  }

  ionViewDidEnter() {
    if (JSON.parse(localStorage.getItem('user'))) {
      //this.user = JSON.parse(localStorage.getItem('user'));
    } else {
      this.router.navigate(['login']);
    }
  }

  async getVisit() {
    this.scheduleService.getVisit().subscribe(
      (res) => {
        this.visitas = res;
      }, (err) => {
        console.log(err);
      }
    )
  }

  async deleteVisit(_id) {
    this.scheduleService.deleteVisit(_id).subscribe(
      (res) => {
        this.getVisit();
      }, (err) => {
        console.log(err);
      }
    )
  }

  updateVisit(visita){
    this.openModal(visita)
  }

  async openModal(visit?) {
    const modal = await this.modalController.create({
      component: ScheduleAddComponent,
      componentProps: {
       visit
      }
    });
    modal.onDidDismiss().then((data) => {
      if (data && data.data) {
        if(data.data.update){
          this.scheduleService.putVisit(data.data).subscribe(
            (res) => {
              this.getVisit();
            }, (err) => {
              console.log(err);
            }
          )
        }else{
          this.scheduleService.postVisit(data.data).subscribe(
            (res) => {
              this.getVisit();
            }, (err) => {
              console.log(err);
            }
          )
        }
      }
    });
    return await modal.present();
  }

  download() {
    const documentDefinition = {
      content: [
        { text: 'Reporte de configuraciones del sistema', style: 'header' },
        ... this.visitas.map((visit) => {

          const clienteD = visit.client.length ? 'Cliente: \n' : ''
          const cliente = visit.client.map(client => (
            `${client.name} - ${client.email}`
          ));

          const supervisorD = visit.supervisor.length ? 'Supervisor: \n' : ''
          const supervisor = visit.supervisor.map(client => (
            `${client.name} - ${client.email}`
          ));

          const tecnicoD = visit.tecnico.length ? 'Tecnico: \n' : ''
          const tecnico = visit.tecnico.map(client => (
            `${client.name} - ${client.email}`
          ));


          const clientes = clienteD + cliente;
          const supervisores = supervisorD + supervisor;
          const tecnicos = tecnicoD + tecnico;
          return {
            text:
              `
              ${clientes}
              ${supervisores}
              ${tecnicos}
              \n    Comenzó la vista: ${visit.startTime} 
          Finalizo la visita: ${visit.endTime}
          Razón: ${visit.reason}
          Coordenadas:
          Latitud: ${visit.coordinates.latitude}
          Longuitud: ${visit.coordinates.longitude}
          `,
            margin: [0, 20, 0, 20]
          }
        })
      ],
      styles: {
        header: { fontSize: 18, bold: true }
      }
    };

    const pdfDocGenerator = pdfMake.createPdf(documentDefinition);

    pdfDocGenerator.getBlob((blob) => {
      saveAs(blob, 'visitas.pdf');
    });
  }

}
