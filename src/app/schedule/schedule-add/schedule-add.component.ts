import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UserService } from '../../shared/user.service'
import { ClientService } from '../../shared/client.service'
import { Geolocation } from '@ionic-native/geolocation/ngx';
import * as L from 'leaflet';

@Component({
  selector: 'app-schedule-add',
  templateUrl: './schedule-add.component.html',
  styleUrls: ['./schedule-add.component.scss'],
})
export class ScheduleAddComponent implements OnInit {

  @Input() visit: any;

  public supervisores: any = [];
  public tecnicos: any = [];
  public clients: any = [];
  public fechaHoraEntrada: Date;
  public fechaHoraSalida: Date;
  map: L.Map;
  selectedCoordinates: L.LatLng;

  constructor(private modalController: ModalController,
    public clientService: ClientService,
    public userService: UserService) {
  }

  ngOnInit() {
    this.getUserTecnicos();
    this.getUserSupervisor();
    this.getClients();
    if(this.visit){
      console.log(this.visit)
      this.fechaHoraEntrada =  this.visit.startTime;
      this.fechaHoraSalida = this.visit.endTime;
    }

    this.map = L.map('map').setView([14.634915, -0.09], 13); 
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(this.map);
  
    this.map.on('click', (event: L.LeafletMouseEvent) => {
      this.selectedCoordinates = event.latlng;
    });

  }

  guardar(supervisor,tecnico, client, reason) {
    this.modalController.dismiss({
      supervisor: supervisor.value,
      tecnico: tecnico.value,
      client: client.value,
      reason: reason.value,
      startTime: this.fechaHoraEntrada,
      endTime: this.fechaHoraSalida,
      duration: 60,
      coordinates: {
        latitude: this.selectedCoordinates.lat,
        longitude: this.selectedCoordinates.lng
      }
    });
  }

  Modificar() {
    this.modalController.dismiss({
      update: true,
      id:this.visit._id,
      startTime: this.fechaHoraEntrada,
      endTime: this.fechaHoraSalida,
      duration: 60
    });
  }

  closeModal(params = {}) {
    this.modalController.dismiss({
    });
  }

  async getUserTecnicos() {
    this.userService.getUsersByRole('tecnico').subscribe(
      (res) => {
        this.tecnicos = res;
      }, (err) => {
        console.log(err);
      }
    )
  }
  async getUserSupervisor() {
    this.userService.getUsersByRole('supervisor').subscribe(
      (res) => {
        this.supervisores = res;
      }, (err) => {
        console.log(err);
      }
    )
  }
  async getClients() {
    this.clientService.getclients().subscribe(
      (res) => {
        this.clients = res;
      }, (err) => {
        console.log(err);
      }
    )
  }
  
  saveStart(){
    this.fechaHoraEntrada = new Date();

  }
  saveEnd(){
    this.fechaHoraSalida = new Date();
  }

  getSelectedCoordinates() {
    if (this.selectedCoordinates) {
      const latitude = this.selectedCoordinates.lat;
      const longitude = this.selectedCoordinates.lng;
      console.log('Latitude:', latitude);
      console.log('Longitude:', longitude);
    }
  }
}
