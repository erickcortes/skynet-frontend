import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ScheduleAddComponent} from './schedule-add.component'
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  declarations: [
    ScheduleAddComponent
  ],
})
export class ScheduleAddModule { }