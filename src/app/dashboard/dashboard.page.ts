import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../shared/authentication-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  user: any = { displayName: '', email: '' };

  constructor(public authService: AuthenticationService,
    public router: Router) {

  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    if (JSON.parse(localStorage.getItem('user'))) {
      this.user = JSON.parse(localStorage.getItem('user'));
    } else {
      this.router.navigate(['login']);
    }
  }
}
