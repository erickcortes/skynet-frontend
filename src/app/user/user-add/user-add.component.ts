import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import {UserService} from '../../shared/user.service'

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss'],
})
export class UserAddComponent implements OnInit{

  public tecnicos:any;

  constructor(private modalController: ModalController,
    public userService: UserService) {
  }

  ngOnInit() {
    this.getClientsByRol();
  }

  guardar(name, email, role, delegatedUsers) {
    this.modalController.dismiss({
      name: name.value,
      email: email.value,
      role: role.value,
      delegatedUsers: delegatedUsers.value
    });
  }
  
  closeModal(params={}) {
    this.modalController.dismiss({
    });
  }

  async getClientsByRol(){
    this.userService.getUsersByRole('tecnico').subscribe(
     (res)=>{
       this.tecnicos = res;
     },(err)=>{
       console.log(err);
     }
    )
   }
}
