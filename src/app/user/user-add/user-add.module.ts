import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserAddComponent} from './user-add.component'
import { IonicModule } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    IonicModule
  ],
  declarations: [
    UserAddComponent
  ],
})
export class UserAddModule { }