import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/user.service'
import { ModalController } from '@ionic/angular';
import { UserAddComponent } from './user-add/user-add.component'
import { Router } from '@angular/router';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit {
  public users: any = [];
  constructor(public userService: UserService,
    private modalController: ModalController,
    public router: Router) {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
  }

  ngOnInit() {
    this.getUsers();
  }
  ionViewDidEnter() {
    if (JSON.parse(localStorage.getItem('user'))) {
      //this.user = JSON.parse(localStorage.getItem('user'));
    } else {
      this.router.navigate(['login']);
    }
  }

  async getUsers() {
    this.userService.getUsers().subscribe(
      (res) => {
        this.users = res;
      }, (err) => {
        console.log(err);
      }
    )
  }

  async deleteUser(email) {
    this.userService.deleteuser(email).subscribe(
      (res) => {
        this.getUsers();
      }, (err) => {
        console.log(err);
      }
    )
  }

  async openModal() {
    const modal = await this.modalController.create({
      component: UserAddComponent
    });
    modal.onDidDismiss().then((data) => {
      if (data && data.data) {
        this.userService.postUser(data.data).subscribe(
          (res) => {
            console.log(res);
            this.getUsers();
          }, (err) => {
            console.log(err);
          }
        )
      }
    });
    return await modal.present();
  }

  download() {
    const documentDefinition = {
      content: [
        { text: 'Reporte de configuraciones del sistema', style: 'header' },
        ... this.users.map((client) => {
          const usuariosD = client.delegatedUsers.length ? '\n Usuarios delegados: \n' : ''
          const usuarios = client.delegatedUsers.map(user => (
            `${user.name} - ${user.email}\n`
          ));
          const concatenar = usuariosD + usuarios
          return {
            text:
              `Nombre: ${client.name} 
          \n Correo: ${client.email}
          \n Role: ${client.role}
          ${concatenar}`,
            margin: [0, 20, 0, 20]
          }
        })
      ],
      styles: {
        header: { fontSize: 18, bold: true }
      }
    };

    const pdfDocGenerator = pdfMake.createPdf(documentDefinition);

    pdfDocGenerator.getBlob((blob) => {
      saveAs(blob, 'usuarios.pdf');
    });
  }

}
