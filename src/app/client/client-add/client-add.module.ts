import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ClientAddPage} from './client-add.page'
import { IonicModule } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    IonicModule
  ],
  declarations: [
    ClientAddPage
  ],
})
export class ClientAddModule { }