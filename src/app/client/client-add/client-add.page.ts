import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-client-add',
  templateUrl: './client-add.page.html',
  styleUrls: ['./client-add.page.scss'],
})
export class ClientAddPage {

  constructor(private modalController: ModalController) {
  }

  guardar(nombre, direccion, phone, email, nota, latitude, longitude) {
    this.modalController.dismiss({
      name: nombre.value,
      address: direccion.value,
      phone: phone.value,
      email: email.value,
      notes: nota.value,
      coordinates: {
        latitude: latitude.value,
        longitude: longitude.value
      }
    });

  }
  
  closeModal(params={}) {
    this.modalController.dismiss({
    });
  }

}
