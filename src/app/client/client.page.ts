import { Component, OnInit } from '@angular/core';
import { ClientService } from '../shared/client.service'
import { ModalController } from '@ionic/angular';
import { ClientAddPage } from './client-add/client-add.page';
import { Router } from '@angular/router';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { saveAs } from 'file-saver';


@Component({
  selector: 'app-client',
  templateUrl: './client.page.html',
  styleUrls: ['./client.page.scss']
})
export class ClientPage implements OnInit {

  public clients: any = [];
  constructor(public clientService: ClientService,
    private modalController: ModalController,
    public router: Router) {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
  }
  ionViewDidEnter() {
    if (JSON.parse(localStorage.getItem('user'))) {
      //this.user = JSON.parse(localStorage.getItem('user'));
    } else {
      this.router.navigate(['login']);
    }
  }

  ngOnInit() {
    this.getClients();
  }

  async getClients() {
    this.clientService.getclients().subscribe(
      (res) => {
        console.log(res);
        this.clients = res;
      }, (err) => {
        console.log(err);
      }
    )
  }

  async deleteClient(email) {
    this.clientService.deleteClients(email).subscribe(
      (res) => {
        this.getClients();
      }, (err) => {
        console.log(err);
      }
    )
  }

  async openModal() {
    const modal = await this.modalController.create({
      component: ClientAddPage
    });
    modal.onDidDismiss().then((data) => {
      if (data && data.data) {
        this.clientService.postClients(data.data).subscribe(
          (res) => {
            console.log(res);
            this.getClients();
          }, (err) => {
            console.log(err);
          }
        )
      }
    });
    return await modal.present();
  }

  download() {
    const documentDefinition = {
      content: [
        { text: 'Reporte de clientes', style: 'header' },
        ... this.clients.map((client) => ({
          text: 
          `Nombre: ${client.name} 
          \n Correo: ${client.email}
          \n Nota del cliente: ${client.nota}
          \n Telefono: ${client.phone}
          \n Dirección: ${client.address}
          \n Coordenadas:
          \n Latitud: ${client.coordinates.latitude}
          \n Longuitud: ${client.coordinates.longuitud}`,
          margin: [0, 20, 0, 20]
        }))
      ],
      styles: {
        header: { fontSize: 18, bold: true }
      }
    };

    const pdfDocGenerator = pdfMake.createPdf(documentDefinition);

    pdfDocGenerator.getBlob((blob) => {
      saveAs(blob, 'clientes.pdf');
    });
  }

}
