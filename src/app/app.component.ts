import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Dashboard', url: '/dashboard', icon: 'reorder-four' },
    { title: 'Clientes', url: '/client', icon: 'people' },
    { title: 'Visitas de clientes', url: '/schedule', icon: 'calendar' },
    { title: 'Configuraciones del sistema', url: '/user', icon: 'settings' },
  ];
  public labels = [];
  constructor() {}
}
