export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyCEl_QXBGSaXpVJeKAhPhVah2U_qX2mils",
    authDomain: "skynet-d3b35.firebaseapp.com",
    databaseURL: "https://skynet-d3b35-default-rtdb.firebaseio.com",
    projectId: "skynet-d3b35",
    storageBucket: "skynet-d3b35.appspot.com",
    messagingSenderId: "159057429535",
    appId: "1:159057429535:web:0e829c9e02a442d861412a",
    measurementId: "G-HE2WHMYSHT"
  },
  baseUrlVisitas:'https://umg-microservice-visitas-fe9cdf02d030.herokuapp.com',
  baseUrlVisitasClientes:'https://umg-microservices-clientes-e4182f833e83.herokuapp.com',
  baseUrlVisitasUsuarios:'https://umg-microservice-usuarios-6f4fd4d83b0b.herokuapp.com'
};
