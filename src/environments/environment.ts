// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCEl_QXBGSaXpVJeKAhPhVah2U_qX2mils",
    authDomain: "skynet-d3b35.firebaseapp.com",
    databaseURL: "https://skynet-d3b35-default-rtdb.firebaseio.com",
    projectId: "skynet-d3b35",
    storageBucket: "skynet-d3b35.appspot.com",
    messagingSenderId: "159057429535",
    appId: "1:159057429535:web:0e829c9e02a442d861412a",
    measurementId: "G-HE2WHMYSHT"
  },
  baseUrlVisitas:'http://localhost:5001',
  baseUrlVisitasClientes:'http://localhost:5002',
  baseUrlVisitasUsuarios:'http://localhost:5003'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
